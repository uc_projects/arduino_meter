
function isDefined(v){
	if(typeof(v) == 'undefined')
		return false;
	return true;
}
function notNull(v){
    if(typeof(v) != 'undefined' && v != null)
        return true;
    return false;
}
function isNull(v){
    if(typeof(v) != 'undefined' && v == null)
        return true;
    return false;
}
function isBool(v){
    if((typeof(v) != 'undefined' && v != null) && typeof(v) == 'boolean')
        return true;
    return false;	
}
function isNumber(v){
    if((typeof(v) != 'undefined' && v != null) && typeof(v) == 'number')
        return true;
    return false;
}
function isNumeric(v){
    if((typeof(v) != 'undefined' && v != null) && typeof(v/1) == 'number')
        return true;
    return false;
}
function isFunction(v){
	if((typeof(v) != 'undefined' && v != null) && typeof(v) == 'function')
		return true;
	return false;
}
function isObject(v){
	if((typeof(v) != 'undefined' && v != null) && typeof(v) == 'object')
		return true;
	return false;
}
function isString(v){
	//return  objectHasFunc(v, 'charAt');
	if((typeof(v) != 'undefined' && v != null) && typeof(v) == 'string')	return true; return false;	
}
function dump(e, _tabs){
	var s = '\t';	
	var tabs = 0;
	if( isDefined(_tabs) && _tabs <= 3) tabs = _tabs; //three levels is max
	for(var i=0;i<tabs;i++) s += s;
	tabs++;
	if( isDefined(e) ){		
		if(isObject(e)){
			for(key in e){ 
				console.log(s + "key: "+key+", type: "+typeof(e[key])+", value:"+e[key]);
				if(isObject(e[key])) dump(e[key], tabs);
			}
		}
		else if( isString(e) ) console.log(s + "type: string, value:"+e);
	} else {
		console.log("type: "+typeof(e));
	}
}
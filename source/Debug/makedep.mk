################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

src\arduino\cores\arduino\abi.cpp

src\arduino\cores\arduino\CDC.cpp

src\arduino\cores\arduino\HardwareSerial.cpp

src\arduino\cores\arduino\HardwareSerial0.cpp

src\arduino\cores\arduino\HardwareSerial1.cpp

src\arduino\cores\arduino\HardwareSerial2.cpp

src\arduino\cores\arduino\HardwareSerial3.cpp

src\arduino\cores\arduino\HID.cpp

src\arduino\cores\arduino\hooks.c

src\arduino\cores\arduino\IPAddress.cpp

src\arduino\cores\arduino\main.cpp

src\arduino\cores\arduino\new.cpp

src\arduino\cores\arduino\Print.cpp

src\arduino\cores\arduino\Stream.cpp

src\arduino\cores\arduino\Tone.cpp

src\arduino\cores\arduino\USBCore.cpp

src\arduino\cores\arduino\WInterrupts.c

src\arduino\cores\arduino\wiring.c

src\arduino\cores\arduino\wiring_analog.c

src\arduino\cores\arduino\wiring_digital.c

src\arduino\cores\arduino\wiring_pulse.c

src\arduino\cores\arduino\wiring_shift.c

src\arduino\cores\arduino\WMath.cpp

src\arduino\cores\arduino\WString.cpp

src\arduino\libraries\EEPROM\EEPROM.cpp

src\arduino\libraries\SPI\SPI.cpp

src\arduino\libraries\U8glib\U8glib.cpp

src\arduino\libraries\U8glib\utility\chessengine.c

src\arduino\libraries\U8glib\utility\u8g_bitmap.c

src\arduino\libraries\U8glib\utility\u8g_circle.c

src\arduino\libraries\U8glib\utility\u8g_clip.c

src\arduino\libraries\U8glib\utility\u8g_com_api.c

src\arduino\libraries\U8glib\utility\u8g_com_api_16gr.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_attiny85_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_common.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_fast_parallel.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_hw_usart_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_no_en_parallel.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_parallel.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_port_d_wr.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_ssd_i2c.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_st7920_custom.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_st7920_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_st7920_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_std_sw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_sw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_t6963.c

src\arduino\libraries\U8glib\utility\u8g_com_arduino_uc_i2c.c

src\arduino\libraries\U8glib\utility\u8g_com_atmega_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_atmega_parallel.c

src\arduino\libraries\U8glib\utility\u8g_com_atmega_st7920_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_atmega_st7920_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_atmega_sw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_i2c.c

src\arduino\libraries\U8glib\utility\u8g_com_io.c

src\arduino\libraries\U8glib\utility\u8g_com_msp430_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_null.c

src\arduino\libraries\U8glib\utility\u8g_com_raspberrypi_hw_spi.c

src\arduino\libraries\U8glib\utility\u8g_com_raspberrypi_ssd_i2c.c

src\arduino\libraries\U8glib\utility\u8g_com_std_sw_spi.c

src\arduino\libraries\U8glib\utility\u8g_cursor.c

src\arduino\libraries\U8glib\utility\u8g_delay.c

src\arduino\libraries\U8glib\utility\u8g_dev_a2_micro_printer.c

src\arduino\libraries\U8glib\utility\u8g_dev_flipdisc_2x7.c

src\arduino\libraries\U8glib\utility\u8g_dev_gprof.c

src\arduino\libraries\U8glib\utility\u8g_dev_ht1632.c

src\arduino\libraries\U8glib\utility\u8g_dev_ili9325d_320x240.c

src\arduino\libraries\U8glib\utility\u8g_dev_ks0108_128x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_lc7981_160x80.c

src\arduino\libraries\U8glib\utility\u8g_dev_lc7981_240x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_lc7981_240x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_lc7981_320x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_ld7032_60x32.c

src\arduino\libraries\U8glib\utility\u8g_dev_null.c

src\arduino\libraries\U8glib\utility\u8g_dev_pcd8544_84x48.c

src\arduino\libraries\U8glib\utility\u8g_dev_pcf8812_96x65.c

src\arduino\libraries\U8glib\utility\u8g_dev_sbn1661_122x32.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1306_128x32.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1306_128x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1306_64x48.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1309_128x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1322_nhd31oled_bw.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1322_nhd31oled_gr.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1325_nhd27oled_bw.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1325_nhd27oled_bw_new.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1325_nhd27oled_gr.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1325_nhd27oled_gr_new.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1327_96x96_gr.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1351_128x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_ssd1353_160x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_64128n.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_dogm128.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_dogm132.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_lm6059.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_lm6063.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_nhd_c12832.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7565_nhd_c12864.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7687_c144mvgd.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7920_128x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7920_192x32.c

src\arduino\libraries\U8glib\utility\u8g_dev_st7920_202x32.c

src\arduino\libraries\U8glib\utility\u8g_dev_t6963_128x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_t6963_128x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_t6963_240x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_t6963_240x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_tls8204_84x48.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1601_c128032.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1608_240x128.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1608_240x64.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1610_dogxl160.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1611_dogm240.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1611_dogxl240.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1701_dogs102.c

src\arduino\libraries\U8glib\utility\u8g_dev_uc1701_mini12864.c

src\arduino\libraries\U8glib\utility\u8g_ellipse.c

src\arduino\libraries\U8glib\utility\u8g_font.c

src\arduino\libraries\U8glib\utility\u8g_font_data.c

src\arduino\libraries\U8glib\utility\u8g_line.c

src\arduino\libraries\U8glib\utility\u8g_ll_api.c

src\arduino\libraries\U8glib\utility\u8g_page.c

src\arduino\libraries\U8glib\utility\u8g_pb.c

src\arduino\libraries\U8glib\utility\u8g_pb14v1.c

src\arduino\libraries\U8glib\utility\u8g_pb16h1.c

src\arduino\libraries\U8glib\utility\u8g_pb16h2.c

src\arduino\libraries\U8glib\utility\u8g_pb16v1.c

src\arduino\libraries\U8glib\utility\u8g_pb16v2.c

src\arduino\libraries\U8glib\utility\u8g_pb32h1.c

src\arduino\libraries\U8glib\utility\u8g_pb8h1.c

src\arduino\libraries\U8glib\utility\u8g_pb8h1f.c

src\arduino\libraries\U8glib\utility\u8g_pb8h2.c

src\arduino\libraries\U8glib\utility\u8g_pb8h8.c

src\arduino\libraries\U8glib\utility\u8g_pb8v1.c

src\arduino\libraries\U8glib\utility\u8g_pb8v2.c

src\arduino\libraries\U8glib\utility\u8g_pbxh16.c

src\arduino\libraries\U8glib\utility\u8g_pbxh24.c

src\arduino\libraries\U8glib\utility\u8g_polygon.c

src\arduino\libraries\U8glib\utility\u8g_rect.c

src\arduino\libraries\U8glib\utility\u8g_rot.c

src\arduino\libraries\U8glib\utility\u8g_scale.c

src\arduino\libraries\U8glib\utility\u8g_state.c

src\arduino\libraries\U8glib\utility\u8g_u16toa.c

src\arduino\libraries\U8glib\utility\u8g_u8toa.c

src\arduino\libraries\U8glib\utility\u8g_virtual_screen.c

src\arduino\libraries\Wire\utility\twi.c

src\arduino\libraries\Wire\Wire.cpp

src\Project\ProjectMain.cpp


Important Note! When charging batteries you must make sure that the charger voltage is less than or equal
 to the battery voltage. For the best battery performance/life you should have them matched. 
 For example: 3.7/4.2V battery and 3.7/4.2V charger: OK - 3.7/4.2V battery and 3.6/4.1V charger: OK
 (but not ideal) - 3.6/4.1V battery and 3.6/4.1V charger: OK - 3.6/4.1V battery and 3.7/4.2V charger NOT OK!
 

L-Ion battery Voltage range: 3-4.2v 3.7v avg

tester is for 30amp cells

4.2v*30a = 126w

3.7v/30a = .1233333ohm

saw an article for battery tester at http://www.instructables.com/id/Arduino-True-Battery-Capacity-Tester-Li-IonNiMH/?ALLSTEPS
after thinking of designing one, it uses a 2.2ohm 10w resistor, I wanted to replace that with a transistor load
that i read about a few years ago.

I was thinking about getting some IMR high drain 18650's from ebay they rateings are
30A discharge 3000mha 3.7v. 30a batteries are recommended value for vaporizers, 2 in parallel
are capable of driving 252watts.

---------- Load Ideas -------------------------

 BJT and heat sink
I could just use some BJT's then wouldn't have to worry about FET GS voltage. I'd have to dissipate
the heat though using a large heatsink which size wize is not ideal plus the needed 12v fan 
also not ideal.

 Resistors
3.7v/30a = .1233333ohm I'd want to use 5 of them and could adjust the load by connecting them in parallel

the best deal i found on ebay was for 10 1ohm 10w resistors for $3.8
http://www.ebay.com/itm/10PCS-10-ohm-10R-J-10-watt-Axial-ceramic-cement-power-resistor-10W-rl610-/301176427336?hash=item461f839348:g:bpoAAOxyc2pTaQES
3.7/1=3.7a
3.7a*3.7v=13.69w = .369% overrating

1ohm/10 = .1ohm
3.7/.1 = 37a = .2333% overrating for 30a battery

 hybrid BJT/resistor
I could place a transistor in series with each resitor. the bjt would be used as a trimmer for the resistor and an on/off switch.
it'd be nice for the trimmer to be digitally controlled but a pot could also work. the circuit would require 10 driver transistors also more than likely

tip120 darlington bjt is cheap has 65W power dissipation 6A collector current. 20pcs $3.74 but needs 120ma base current
heatsinks are $1 each for to-220 available through ebay seller "wedoheatsink2013"

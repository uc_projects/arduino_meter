

//bluetooth -----------------------------------------------------
int responseTime = 850;

void print_response(){
  delay(responseTime);
  //Serial1.available();
  String response = Serial1.readString();
  Serial.print(response);
  Serial.print("\n");  
}
void start_bt_at(){
  Serial1.print("AT");
  Serial1.flush();

  Serial.print("AT: ");
  print_response();
}
void get_bt_version(){    
  Serial1.print("AT+VERSION");
  Serial1.flush();
  Serial.print("get_bt_version: ");
  print_response();  
}

void set_bt_baud(int baud){
  String at_change_baud = String("AT+BAUD");
  switch(baud){
    case 1200:
      at_change_baud.concat("1");
      break;
    case 2400:
      at_change_baud.concat("2");
      break;
    case 4800:
      at_change_baud.concat("3");
      break;
    case 9600:
      at_change_baud.concat("4");
      break;
    case 19200:
      at_change_baud.concat("5");
      break;
    case 38400:
      at_change_baud.concat("6");
      break;
    case 57600:
      at_change_baud.concat("7");
      break;
    case 115200:
      at_change_baud.concat("8");
      break;
    case 230400:
      at_change_baud.concat("9");
      break;
    case 460800:
      at_change_baud.concat("A");
      break;
    case 921600:
      at_change_baud.concat("B");
      break;
    case 1382400:
      at_change_baud.concat("C");
      break;
  }
  Serial.print(at_change_baud);
  Serial1.print(at_change_baud);
  Serial1.flush();            //wait for bytes to be sent
  Serial1.end();              //restart @ baud
  Serial1.begin(baud);
 
  Serial.print("set_bt_baud: ");      
  print_response();
}
void set_bt_identifier(String identifier){
  String at_change_identifier = String("AT+NAME");
  at_change_identifier.concat(identifier);

  Serial1.print(at_change_identifier);
  print_response();
}
//default is 1234
void set_bt_pin(String pin){
  String at_change_identifier = String("AT+PIN");
  at_change_identifier.concat(pin);

  Serial1.print(at_change_identifier);

  Serial.print("set_bt_pin: ");
  print_response();
}


void setup(){
  delay(1500);
  Serial.begin(115200);
  //have to initalize to default 9600
  Serial1.begin(115200);

  start_bt_at();
  get_bt_version();
  //set_bt_identifier(identifier);
  //set_bt_pin("4545");
  //if(baud != 9600)
  //  set_bt_baud(baud);
  //serial_set_timeout(bt_port, 1000); //set time out for waiting for read data
}


void loop(){
  //while(Serial1.available() > 0){
    //bt_buff[bt_offset] = Serial1.read();
  //}
}
